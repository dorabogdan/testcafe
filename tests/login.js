import { Selector } from 'testcafe';

fixture('Login into the application')
    .page('https://www.ejobs.ro')

test('Check if page loaded', async t => {
    await t
        .expect(Selector('.logo').visible).ok()
        .expect(Selector('.menus').visible).ok()
        .expect(Selector('.nPrezi').visible).ok()
        .expect(Selector('.login-menu').textContent).contains('Intră în cont')
})

test('Login', async t => {
    await t
        .expect(Selector('.login-menu').textContent).contains('Intră în cont')
        .click('.menu-item-login > a[href="https://www.ejobs.ro/login/"]')
        .click('#login-switch-user')
        .expect(Selector('#login-user').visible).ok()
        .typeText('[placeholder="Email"]', 'dorabogdan97@yahoo.com')
        .typeText('[placeholder="Parola"]', 'Testcafe123')
        .click('[type="submit"]')
        .expect(Selector('.name').textContent).contains('Dora Bogdan')
        .expect(Selector('.contact').textContent).contains('dorabogdan97@yahoo.com')
        .expect(Selector('.avatar').visible).ok()
        .expect(Selector('.cv-main').visible).ok()
        .expect(Selector('.account-menu').visible).ok()
        .click('.account-menu')
        .expect(Selector('.edropdown-menu > li > .name').textContent).contains('Dora Bogdan')
        .expect(Selector('.edropdown-menu > li > .mail').textContent).contains('dorabogdan97@yahoo.com')
        .expect(Selector('.edropdown-menu > li > a').textContent).contains('Editare CV')
        .expect(Selector('.edropdown-menu > li:nth-child(3) > a').textContent).contains('Setări cont')
        .expect(Selector('.edropdown-menu > li:nth-child(4) > a').textContent).contains('Aplicările mele')
        .expect(Selector('.edropdown-menu > li:nth-child(5) > a').textContent).contains('Joburi salvate')
        .expect(Selector('.edropdown-menu > li:nth-child(6) > a').textContent).contains('Interviuri programate')
        .expect(Selector('.edropdown-menu > li:nth-child(7) > a').textContent).contains('Scrisori de intenţie')
        .expect(Selector('.edropdown-menu > li:nth-child(8) > a').textContent).contains('Setări alerte email')
        .expect(Selector('.edropdown-menu > li > .name-lang').textContent).contains('Limbă site')
        .expect(Selector('.edropdown-menu > li > a[href="https://www.ejobs.ro/user/logout.php"]').textContent).contains('Ieșire cont')

})