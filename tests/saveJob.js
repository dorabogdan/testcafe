import { Selector } from 'testcafe';

fixture('Save job feature')
    .page('https://www.ejobs.ro/login/')
    .beforeEach(async t => {
        await t
            .click('#login-switch-user')
            .expect(Selector('#login-user').visible).ok()
            .typeText('[placeholder="Email"]', 'dorabogdan97@yahoo.com')
            .typeText('[placeholder="Parola"]', 'Testcafe123')
            .click('[type="submit"]')

    })

test('Search and save job', async t => {
    await t
        .navigateTo('https://www.ejobs.ro/locuri-de-munca/')
        .click('.main-menu > .menu-item:nth-child(1)')
        .typeText('.search-input', 'medicina')
        .click('#jobsListSearch')
        .wait(1000)
        .click('.jobitem-title:nth-child(2)')
        .click('.jobad-btn-save')

})

test('Check saved job', async t => {
    await t
    .click('.account-menu')
    .click('.edropdown-menu > li:nth-child(5)')
    .expect(Selector('.page-title').textContent).contains('Joburi salvate')
    .expect(Selector('.fitem-list').visible).ok()

})