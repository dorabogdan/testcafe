import { Selector } from 'testcafe';

fixture('Career page')
    .page('https://www.ejobs.ro')

test('Go to career page & check it', async t => {
    await t
        .click('.menu-item > a[href="//cariera.ejobs.ro/"]')
        .expect(Selector('.entry-content').visible).ok()
        .click('.icon-menu-fine')
        .expect(Selector('.menu > li > a').textContent).contains('Coronavirus Romania')
        .expect(Selector('.menu > li > a').textContent).contains('Găsește jobul')
        .expect(Selector('.menu > li > a').textContent).contains('Obține jobul')
        .expect(Selector('.menu > li > a').textContent).contains('Trăiește jobul')
        .expect(Selector('.menu > li > a').textContent).contains('Evoluează')
        .expect(Selector('.menu > li > a').textContent).contains('Primul Job')
        .expect(Selector('.menu > li > a').textContent).contains('Accesează eJobs')

})