import { Selector } from 'testcafe';

fixture('Search job')
    .page('https://www.ejobs.ro/locuri-de-munca/')

test('Search job after key words', async t => {
    await t
        .expect(Selector('#search_jobs').visible).ok()
        .expect(Selector('#searchHeader').visible).ok()
        .expect(Selector('#searchBar').visible).ok()
        .typeText('.search-input', 'IT')
        .click('#jobsListSearch')
        .wait(1000)
        .expect(Selector('.subtitle:nth-child(3)').textContent).contains('IT')
        .expect(Selector('.jobitem-title').textContent).contains('IT')

})

test('Search job after city', async t => {
    await t
        .click('.search-btn-filter')
        .click('#filter-city > div > label:nth-child(4)')
        .click('.closeFilters')
        .wait(1000)
        .expect(Selector('.subtitle:nth-child(3)').textContent).contains('Cluj-Napoca')
        .expect(Selector('.jobitem-location').textContent).contains('Cluj-Napoca')

})

test('Search job after department and experience', async t => {
    await t
        .click('.search-btn-filter')
        .click('#filter-department > div > label:nth-child(3)')
        .click('#filter-career > div > label:nth-child(2)')
        .click('.closeFilters')
        .wait(1000)
        .expect(Selector('.subtitle:nth-child(3)').textContent).contains('Agricultura')
        .expect(Selector('.subtitle:nth-child(3)').textContent).contains('Entry-Level')

})

test('Search job after industry, education and job type', async t => {
    await t
        .click('.search-btn-filter')
        .click('#filter-industry > div > label:nth-child(1)')
        .click('#filter-education > div > label:nth-child(3)')
        .click('#filter-type > div > label:nth-child(1)')
        .click('.closeFilters')
        .wait(1000)
        .expect(Selector('.subtitle:nth-child(3)').textContent).contains('Administratie')
        .expect(Selector('.subtitle:nth-child(3)').textContent).contains('Student')
        .expect(Selector('.subtitle:nth-child(3)').textContent).contains('Full time')

})