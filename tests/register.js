import { Selector } from 'testcafe';

fixture('Create new account')
    .page('https://www.ejobs.ro/user/inregistrare')

test('Registration', async t => {
    await t
        .click('#readmore-register')
        .click('#accept-register')
        .expect(Selector('.mainTitle').textContent).contains('Creează-ți gratuit contul eJobs')
        .expect(Selector('.formWrapper').visible).ok()
        .typeText('[name="name"]', 'Dora')
        .typeText('[name="surname"]', 'Bogdan')
        .typeText('[name="email"]', 'dorabogdan97@gmail.com')
        .typeText('[name="pass"]', 'Testcafe123')
        .typeText('[name="oras"]', 'Cluj-Napoca')
        .typeText('#birthDate', '27/05/1997')
        .click('[type="submit"]')
        .expect(Selector('.page-register-candidat-step-4').visible).ok()
        .click('.material-input-wrapper > label:nth-child(2)')
        .click('[type="submit"]')
        .expect(Selector('.page-register-candidat-confirmare').visible).ok()
        

})

